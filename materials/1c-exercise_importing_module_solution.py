"""Solutions to first exercise on import modules

Author
------
Jenni Rinker
rink@dtu.dk
"""

import hello_world
import hello_world as hw
from hello_world import greetings
from hello_world import greetings as gr